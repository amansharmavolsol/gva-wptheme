<?php
$default_cover_title = get_bloginfo( 'name' );
$default_cover_lead = get_bloginfo( 'description' );
?>


<header class="hero py-5  wp-bs-4-jumbotron" role="banner">
    <div class="container d-flex align-items-center">
        <div class="row">
            <div class="d-flex align-items-center flex-wrap text-white justify-content-center px-3 px-lg-0">
                <h1 class="title1"><?php echo wp_kses_post( get_theme_mod( 'front_cover_title', $default_cover_title ) ); ?></h1>
                <p class="w-100 text-center"><?php echo wp_kses_post( get_theme_mod( 'front_cover_lead', $default_cover_lead ) ); ?></p>
                <?php if( get_theme_mod( 'front_cover_btn_text' ) ) : ?><a href="<?php echo esc_url( get_theme_mod( 'front_cover_btn_link' ) ); ?>" class="btn btn-primary btn-lg">
                    <?php echo esc_html( get_theme_mod( 'front_cover_btn_text' ) ); ?></a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</header>
