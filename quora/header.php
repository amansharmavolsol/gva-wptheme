<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Quora
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<aside class="aside-nav" >
    <button class="btn btn-dark btn-sm">
        <i class="fa fa-align-left"></i>
    </button>
    <!--<ul class="d-flex flex-wrap align-items-center justify-content-center">
        <li>
            <a href="#">
                <i class="fa fa-times"></i>
                <span>Trips</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-arrow-right"></i>
                <span>Where We Go?</span>
            </a>
        </li>
    </ul>-->
    <?php
    wp_nav_menu( array(
        'theme_location'  => 'menu-1',
        'menu_id'         => 'primary-menu',
        'menu_class'      => 'cus-navbar d-flex flex-wrap align-items-center justify-content-center',
        'fallback_cb'     => '__return_false',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           =>3,
        'walker'          => new WP_bootstrap_4_walker_nav_menu()
    ) );
    ?>
</aside>


<div id="wrapper" class="site">
    <a class="sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'quora' ); ?></a>
    <nav class="navbar navbar-expand-lg shadow-sm px-3" role="navigation">
        <div class="container-fluid navbar-border px-lg-0">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <?php the_custom_logo(); ?>
            </div>
            <form role="search" method="get" class="m-auto w-50 header-search d-none d-lg-inline-block"  action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="form-group searchinput mb-0">
                <input type="search" role="search" class="s form-control" name="s" placeholder="<?php esc_attr_e( 'Search&hellip;', 'quora' ); ?>" value="<?php the_search_query(); ?>" >
                <button class="search-btn" type="submit" role="button">
                    <i class="fa fa-search text-white"></i>
                </button>
                </div>
            </form>

            <ul class="m-l-auto mb-0 list-unstyled d-lg-flex align-items-center d-none">
                <li class="px-3">
                    <div class="text-center text-dark">
                        <small class="d-block font-weight-bold">Call Us</small>
                        <b class="h6">1800-230-120-1220</b>
                    </div>
                </li>
                <li class="nav-item dropdown px-3">
                    <?php
                      $current_user = wp_get_current_user();
                         if ( ! $current_user->exists() ) {
                        ?>
                             <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                 <i class="fa fa-lock fa-1x"></i> My Account
                             </a>
                    <div class="dropdown-menu bg-white shadow-lg border-0 dropdown-menu-right">
                        <a class="dropdown-item" href="<?php echo home_url(); ?>/my-account/">
                           Login Account
                        </a>
                        <a class="dropdown-item" href="<?php echo home_url(); ?>/my-account/">
                          Free Signup
                        </a>
                    </div>
                             <?php
                         }else{?>

                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user fa-1x"></i>
                        <?php
                        echo $current_user->display_name;
                        ?>
                    </a>
                    <div class="dropdown-menu bg-white shadow-lg border-0 dropdown-menu-right">
                        <a class="dropdown-item" href="<?php echo home_url(); ?>/my-account/">
                           My Account
                        </a>
                        <a class="dropdown-item" href="<?php echo home_url(); ?>/my-account/orders/">
                            Orders
                        </a>
                        <a class="dropdown-item" href="<?php echo home_url(); ?>/my-account/edit-account/">
                            Edit Account
                        </a>
                        <a class="dropdown-item" href="<?php echo wp_logout_url(); ?>">
                            Logout
                        </a>
                    </div>
                    <?php
                     // $userName = $current_user->display_name;
                     /*echo 'Username: ' . $current_user->user_login . '<br />';
                     echo 'User email: ' . $current_user->user_email . '<br />';
                     echo 'User first name: ' . $current_user->user_firstname . '<br />';
                     echo 'User last name: ' . $current_user->user_lastname . '<br />';
                     echo 'User display name: ' . $current_user->display_name . '<br />';
                     echo 'User ID: ' . $current_user->ID . '<br />';*/
                         }
                    ?>
                </li>
            </ul>

        </div><!-- /.container-fluid -->
    </nav>


	<div id="content" class="site-content">
